# Code written by Oladeji Sanyaolu (Keyboard Controlled Motor L298) 2/3/2019

import sys, tty, termios, time
import RPi.GPIO as GPIO

mode = GPIO.getmode()

ForeRight = 18 # Pin 22
BackRight = 17 # Pin 11
ForeLeft  = 24 # Pin 18
BackLeft  = 23 # Pin 16

goFore  = False
goBack  = False
goLeft  = False
goRight = False

class MotorControl:
    def __init__(self):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(ForeRight, GPIO.OUT)
        GPIO.setup(BackRight, GPIO.OUT)
        GPIO.setup(ForeLeft, GPIO.OUT)
        GPIO.setup(BackLeft, GPIO.OUT)
        pass

    def forward(speed=100):
        global goFore, goBack, goLeft, goRight

        if (not goFore):
            MotorControl.stop(0)
            goFore  = True
            goBack  = False
            goLeft  = False
            goRight = False

        GPIO.output(ForeLeft, GPIO.HIGH)
        GPIO.output(ForeRight, GPIO.HIGH)
        pass
    
    def backward(speed=100):
        global goFore, goBack, goLeft, goRight

        if (not goBack):
            MotorControl.stop(0)
            goFore  = False
            goBack  = True
            goLeft  = False
            goRight = False

        GPIO.output(BackLeft, GPIO.HIGH)
        GPIO.output(BackRight, GPIO.HIGH)
        pass

    def leftward(speed=100):
        global goFore, goBack, goLeft, goRight

        if (not goLeft):
            MotorControl.stop(0)
            goFore  = False
            goBack  = False
            goLeft  = True
            goRight = False

        GPIO.output(ForeRight, GPIO.HIGH)
        pass

    def rightward(speed=100):
        global goFore, goBack, goLeft, goRight

        if (not goRight):
            MotorControl.stop(0)
            goFore  = False
            goBack  = False
            goLeft  = False
            goRight = True

        GPIO.output(ForeLeft, GPIO.HIGH)
    
    @staticmethod
    def stop(null):
        GPIO.output(ForeLeft, GPIO.LOW)
        GPIO.output(BackLeft, GPIO.LOW)
        GPIO.output(ForeRight, GPIO.LOW)
        GPIO.output(BackRight, GPIO.LOW)
        pass

    def destroy():
        MotorControl.stop(0)
        GPIO.cleanup()
        pass

if __name__ == '__main__':
    pass
